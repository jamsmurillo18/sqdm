package com.comicvine;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;

import java.io.IOException;
import java.io.FileDescriptor;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;

import com.comicvine.Tools.Animations;
import com.comicvine.Tools.CircledImageView;
import com.comicvine.Tools.CustomPrompt;
import com.comicvine.Tools.Preference;
import android.content.Context;
import android.content.ContextWrapper;

public class EditUser extends AppCompatActivity {

    private EditText etName, etLast, etPass, etPass2, etEmail;
    private CircledImageView circledImageView;
    FloatingActionButton fab;

    Bitmap resizedBitmap;
    String picturePath;
    FileDescriptor fileDescriptor;

    private Preference preference = new Preference();
    Animations animations = new Animations();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_user);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Typeface myFontT = Typeface.createFromAsset(getAssets(), "fonts/calibri_bold_italic.ttf");
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setTypeface(myFontT);

        declareAll();
        setAllTexts();
        animations.animateFAB(getBaseContext(), fab);

        circledImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, 1);
            }
        });

        String filePath = preference.getUserProfilePicturePath(getSupportActionBar().getThemedContext());
        File dir = new File(filePath);
        if(dir.exists())
            circledImageView.setImageBitmap(BitmapFactory.decodeFile(filePath));

        fab.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if (allIsCorrect()) {

                    /*ConnectivityManager connMgr = (ConnectivityManager)
                            getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

                    if (networkInfo != null && networkInfo.isConnected()) {

                        wireConnection = new updateUserService();
                        wireConnection.execute(CALL,
                                etId.getText().toString(),
                                etName.getText().toString(),
                                etLast.getText().toString(),
                                userTypeTv.getText().toString(),
                                etPass.getText().toString(),
                                etCareer.getText().toString(),
                                etTel.getText().toString(),
                                etEmail.getText().toString(),
                                preference.getUserParam(getSupportActionBar().getThemedContext(),9));

                    } else {
                        new CustomPrompt().myToast(getSupportActionBar().getThemedContext(),
                                "No es posible actualizar tus datos. Verifica tu conexión Wi-Fi o la disponibilidad de tu plan de datos",0,false);
                    }*/

                } else {

                    new CustomPrompt().myToast(getSupportActionBar().getThemedContext(),
                            "Por favor, llena todos los campos", 0, false);
                }
            }
        });
    }

    public void setAllTexts() {


        etName.setText(preference.getUserParam(getSupportActionBar().getThemedContext(), 1));
        etLast.setText(preference.getUserParam(getSupportActionBar().getThemedContext(), 2));
        etPass.setText(preference.getUserParam(getSupportActionBar().getThemedContext(), 4));
        etPass2.setText("");
        etEmail.setText(preference.getUserParam(getSupportActionBar().getThemedContext(), 3));
    }

    public void declareAll() {

        circledImageView = (CircledImageView) findViewById(R.id.profile_pic);
        etName = (EditText) findViewById(R.id.name_et);
        etLast = (EditText) findViewById(R.id.last_et);
        etPass = (EditText) findViewById(R.id.password_et);
        etPass2 = (EditText) findViewById(R.id.password2_et);
        etEmail = (EditText) findViewById(R.id.doc_et);

        fab = (FloatingActionButton) findViewById(R.id.fab);
    }

    private boolean allIsCorrect() {

        boolean isCorrect = true;

        if (etName.getText().toString().length() == 0)
            isCorrect = (isCorrect && false);

        if (etLast.getText().toString().length() == 0)
            isCorrect = (isCorrect && false);

        if (etPass.getText().toString().length() == 0)
            isCorrect = (isCorrect && false);
        else if (etPass2.getText().toString().length() == 0)
            isCorrect = (isCorrect && false);
        else if (!etPass.getText().toString().equals(etPass2.getText().toString())) {

            etPass2.setText("");
            etPass.setText("");
            new CustomPrompt().myToast(getSupportActionBar().getThemedContext(),
                    "Las contraseñas no coinciden, ingrese de nuevo la contraseña actual.", 0, false);
            isCorrect = (isCorrect && false);

        } else if (!etPass.getText().toString().equals(preference.getUserParam(getSupportActionBar().getThemedContext(), 4))) {

            etPass2.setText("");
            etPass.setText("");
            new CustomPrompt().myToast(getSupportActionBar().getThemedContext(),
                    "La contraseña que ingresaste no coincide con la contraseña actual del usuario.", 0, false);
            isCorrect = (isCorrect && false);
        }

        if (etPass2.getText().toString().length() == 0)
            isCorrect = (isCorrect && false);

        if (etEmail.getText().toString().length() == 0)
            isCorrect = (isCorrect && false);

        return isCorrect;
    }

    private Bitmap getBitmapFromUri(Uri uri) throws IOException {

        ParcelFileDescriptor parcelFileDescriptor = getContentResolver().openFileDescriptor(uri, "r");
        fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();

        return image;
    }

    public Bitmap rotate(Bitmap bitmap, int angle, boolean right){

        Bitmap rotated;

        Matrix matrix = new Matrix();

        if(right)
            matrix.postRotate(angle);
        else
            matrix.postRotate(angle*(-1));

        rotated = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);

        Canvas canvas = new Canvas(rotated);
        Paint paint = new Paint();
        paint.setFilterBitmap(true);

        return rotated;
    }

    public Bitmap scaleDown(Bitmap bitmap, float maxImageSize, boolean filter) {

        float ratio = Math.min(
                (float) maxImageSize / bitmap.getWidth(),
                (float) maxImageSize / bitmap.getHeight());

        int width = Math.round((float) ratio * bitmap.getWidth());
        int height = Math.round((float) ratio * bitmap.getHeight());

        Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, width, height, filter);

        return scaledBitmap;
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        int rotationInDegrees = -1;

        if (requestCode == 1 && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();

            Bitmap bmp = null;
            try {

                bmp = getBitmapFromUri(selectedImage);

            } catch (IOException e) {
                e.printStackTrace();
            }

            circledImageView.setImageBitmap( rotate(scaleDown(bmp, 288, true), 0, true) );

            String ruta = guardarImagen(getSupportActionBar().getThemedContext(), "imagen", bmp);
            preference.setUserProfilePicturePath(getSupportActionBar().getThemedContext(), ruta);
        }
    }

    private String guardarImagen (Context context, String nombre, Bitmap imagen){

        ContextWrapper cw = new ContextWrapper(context);
        File dirImages = cw.getDir("Imagenes", Context.MODE_PRIVATE);
        File myPath = new File(dirImages, nombre + ".png");

        FileOutputStream fos = null;
        try{
            fos = new FileOutputStream(myPath);
            imagen.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
        }catch (FileNotFoundException ex){
            ex.printStackTrace();
        }catch (IOException ex){
            ex.printStackTrace();
        }
        return myPath.getAbsolutePath();
    }
}
