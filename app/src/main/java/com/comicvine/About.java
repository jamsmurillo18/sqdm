package com.comicvine;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

public class About extends AppCompatActivity {

    private TextView tv_name, tv_version;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        getSupportActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        Typeface myFontT = Typeface.createFromAsset(getAssets(), "fonts/calibri_bold_italic.ttf");

        tv_name = (TextView) findViewById(R.id.tv_name);
        tv_name.setTypeface(myFontT);

        tv_version = (TextView) findViewById(R.id.tv_version);
        tv_version.setText("Versión "+BuildConfig.VERSION_NAME);
    }
}
