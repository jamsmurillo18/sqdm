package com.comicvine;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class Detail extends AppCompatActivity {

    CollapsingToolbarLayout collapsingToolbarLayout;
    ImageView imageView;
    TextView title, desc;
    FloatingActionButton fab;

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        imageView = (ImageView) findViewById(R.id.image);
        title = (TextView)findViewById(R.id.title);
        desc = (TextView)findViewById(R.id.description);
        fab = (FloatingActionButton) findViewById(R.id.fab);

        collapsingToolbarLayout.setTitle(getIntent().getStringExtra("name"));
        collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));

        Picasso.with(getSupportActionBar().getThemedContext())
                .load(getIntent().getStringExtra("imgUrl"))
                .resize(432,640)
                .into(imageView);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {

            title.setText(Html.fromHtml(getIntent().getStringExtra("name"), Html.FROM_HTML_MODE_LEGACY));
            desc.setText(Html.fromHtml(getIntent().getStringExtra("description")
                    +"<br><br><b>Calificación</b>: "+getIntent().getStringExtra("rating")
                    +"<br><br><b>Lanzamiento</b>: "+getIntent().getStringExtra("release_date")
                    +"<br><br><b>Duración</b>: "+getIntent().getStringExtra("runtime") + " min"
                    +"<br><br><b>Ingresos</b>: $ "+getIntent().getStringExtra("revenues"), Html.FROM_HTML_MODE_LEGACY));

        } else {

            title.setText(Html.fromHtml(getIntent().getStringExtra("name")));
            desc.setText(Html.fromHtml(getIntent().getStringExtra("description")
                    +"<br><br><b>Calificación</b>: "+getIntent().getStringExtra("rating")
                    +"<br><br><b>Lanzamiento</b>: "+getIntent().getStringExtra("release_date")
                    +"<br><br><b>Duración</b>: "+getIntent().getStringExtra("runtime")+ " min"
                    +"<br><br><b>Ingresos</b>: $ "+getIntent().getStringExtra("revenues") ));

        }

        fab.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                // Mark as favorite
                // Change icon state

            }
        });
    }
}
