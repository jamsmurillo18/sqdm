package com.comicvine;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.comicvine.Tools.CustomPrompt;
import com.comicvine.Tools.Preference;

public class Register extends AppCompatActivity {

    private EditText nameEt, lastEt, passwordEt, passwordEt_2, emailEt;
    private Button register;

    private Preference preference = new Preference();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Typeface myFontT = Typeface.createFromAsset(getAssets(), "fonts/calibri_bold_italic.ttf");
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setTypeface(myFontT);

        declareAll();
        setAllTexts();

        register.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if(allIsCorrect()){

                    // Registro

                        preference.setUserParam(getSupportActionBar().getThemedContext(),
                                nameEt.getText().toString(),
                                lastEt.getText().toString(),
                                emailEt.getText().toString(),
                                passwordEt.getText().toString());

                        new CustomPrompt().myToast(getSupportActionBar().getThemedContext(),"La cuenta de "
                                .concat(preference.getUserParam(getSupportActionBar().getThemedContext(), 1))
                                .concat(" fue creada correctamente, ahora inicia sesión con tu correo y contraseña"),0,false);
                        finish();

                } else {

                    new CustomPrompt().myToast(getSupportActionBar().getThemedContext(),
                            "Complete todos los campos por favor",0,false);
                }
            }
        });
    }

    public void setAllTexts(){

        nameEt.setText(preference.getUserParam(getSupportActionBar().getThemedContext(),1));
        lastEt.setText(preference.getUserParam(getSupportActionBar().getThemedContext(),2));
        passwordEt.setText(preference.getUserParam(getSupportActionBar().getThemedContext(),4));
        passwordEt_2.setText("");
        emailEt.setText(preference.getUserParam(getSupportActionBar().getThemedContext(),3));
    }

    public void declareAll(){

        nameEt = (EditText)findViewById(R.id.name_et);
        lastEt = (EditText)findViewById(R.id.last_et);
        passwordEt = (EditText)findViewById(R.id.password_et);
        passwordEt_2 = (EditText)findViewById(R.id.password2_et);
        emailEt = (EditText)findViewById(R.id.doc_et);

        register = (Button)findViewById(R.id.update_but);
    }

    private boolean allIsCorrect(){

        boolean isCorrect = true;

        if (nameEt.getText().toString().length()==0)
            isCorrect = (isCorrect && false);

        if (lastEt.getText().toString().length()==0)
            isCorrect = (isCorrect && false);

        if(passwordEt.getText().toString().length()==0)
            isCorrect = (isCorrect && false);
        else if(passwordEt_2.getText().toString().length()==0)
            isCorrect = (isCorrect && false);
        else if(!passwordEt.getText().toString().equals(passwordEt_2.getText().toString())){

            passwordEt_2.setText("");
            passwordEt.setText("");
            new CustomPrompt().myToast(getSupportActionBar().getThemedContext(),
                    "Las contraseñas no coinciden, ingrese de nuevo la contraseña.",0,false);
            isCorrect = (isCorrect && false);

        }

        if(passwordEt_2.getText().toString().length()==0)
            isCorrect = (isCorrect && false);

        if(emailEt.getText().toString().length()==0)
            isCorrect = (isCorrect && false);

        return isCorrect;
    }

    private String capitalize(String s) {

        if (s == null || s.length() == 0)
            return "";

        char first = s.charAt(0);

        if (Character.isUpperCase(first))
            return s;
        else
            return Character.toUpperCase(first) + s.substring(1);
    }
}
