package com.comicvine.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.comicvine.EditUser;
import com.comicvine.R;
import com.comicvine.Tools.CircledImageView;
import com.comicvine.Tools.Preference;
import android.provider.MediaStore;
import android.graphics.BitmapFactory;

import java.io.File;

public class Fragment_account extends Fragment {

    private TextView nameTv, emailTv;
    private Button editBut;
    private Preference preference = new Preference();
    private CircledImageView circledImageView;
    
    public Fragment_account() {
        // Required empty public constructor
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View v = inflater.inflate(R.layout.fragment_account, container, false);

        circledImageView = (CircledImageView) v.findViewById(R.id.profile_pic);
        nameTv = (TextView)v.findViewById(R.id.name_tv);
        emailTv = (TextView)v.findViewById(R.id.email_tv);
        editBut = (Button)v.findViewById(R.id.edit_but);

        String filePath = preference.getUserProfilePicturePath(v.getContext());
        File dir = new File(filePath);
        if(dir.exists())
            circledImageView.setImageBitmap(BitmapFactory.decodeFile(filePath));

        nameTv.setText(preference.getUserParam(v.getContext(),1)
                + " " + preference.getUserParam(v.getContext(),2));
        emailTv.setText(preference.getUserParam(v.getContext(),3));

        editBut.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent update = new Intent(v.getContext(),EditUser.class);
                startActivity(update);
            }
        });
        
        return v;
    }
}
