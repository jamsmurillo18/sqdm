package com.comicvine.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import com.comicvine.Adapters.RecyclerForFavorites;
import com.comicvine.R;
import com.comicvine.Tools.Animations;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Fragment_favorites extends Fragment {

    List<String> descriptions = new ArrayList<>();
    List<String> ids = new ArrayList<>();
    List<String> names = new ArrayList<>();
    List<String> producers = new ArrayList<>();
    List<String> ratings = new ArrayList<>();
    List<String> releaseDates = new ArrayList<>();
    List<String> runtimes = new ArrayList<>();
    List<String> smallIconUrls = new ArrayList<>();
    List<String> studios = new ArrayList<>();
    List<String> revenues = new ArrayList<>();
    List<String> writers = new ArrayList<>();

    RecyclerForFavorites adapter;
    RecyclerView recyclerView;
    Animations animations = new Animations();

    Context context;
    getMovieService wireConnection;
    String CALL = "https://comicvine.gamespot.com/api/movies/?api_key=b92fe4a1f037e397515a6b759239eaee558eef53&format=json";

    public Fragment_favorites() { }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View v = inflater.inflate(R.layout.fragment_favorites, container, false);
        context = v.getContext();

        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        int noOfColumns = (int) (dpWidth / 160);

        recyclerView = (RecyclerView) v.findViewById(R.id.recycler);
        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(noOfColumns+1, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);

        ConnectivityManager connMgr = (ConnectivityManager)
                v.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {

            wireConnection = new getMovieService();
            wireConnection.execute(CALL);
        }

        return v;
    }

    public class getMovieService extends AsyncTask<String, Void, String> {

        int error;
        ProgressDialog progressDialog;

        protected String doInBackground(String... params) {

            URL url = null ;

            try{
                url = new URL(params[0]);

                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestProperty("Accept-Encoding","identity");

                int rta = connection.getResponseCode();
                StringBuilder result = new StringBuilder();

                error = 3;

                if(rta == HttpURLConnection.HTTP_OK){

                    InputStream in = new BufferedInputStream(connection.getInputStream());
                    BufferedReader reader = new BufferedReader(new InputStreamReader(in));

                    String line;
                    while ((line = reader.readLine()) != null){
                        result.append(line);
                    }

                    JSONObject respuestaJSON = new JSONObject(result.toString());
                    String resultJSON = respuestaJSON.getString("error");

                    if(resultJSON.equals("OK")){

                        error = 4;
                        descriptions.clear();
                        ids.clear();
                        names.clear();
                        producers.clear();
                        ratings.clear();
                        releaseDates.clear();
                        runtimes.clear();
                        smallIconUrls.clear();
                        studios.clear();
                        revenues.clear();
                        writers.clear();

                        JSONArray results = respuestaJSON.getJSONArray("results");

                        if(results != null){

                            for(int i=0; i<results.length(); i++){

                                descriptions.add(i,results.getJSONObject(i).getString("description"));
                                ids.add(i, String.valueOf(results.getJSONObject(i).getInt("id")));
                                names.add(i,results.getJSONObject(i).getString("name"));

                                /*JSONArray producersArray = results.getJSONObject(i).getJSONArray("producers");
                                //JSONArray producersArray = new JSONArray(results.getJSONObject(i).getString("producers"));

                                if(producersArray.length()>0){

                                    String s="";
                                    for(int j=0; j<producersArray.length(); j++){

                                        JSONObject producerObject = producersArray.getJSONObject(j);

                                        if(producerObject != null){

                                            s.concat( producerObject.getString("name")).concat(", ");

                                        } else
                                            producers.add(i,"None");
                                    }

                                } else
                                    producers.add(i,"None");*/

                                ratings.add(i,results.getJSONObject(i).getString("rating"));
                                releaseDates.add(i,results.getJSONObject(i).getString("release_date"));
                                runtimes.add(i,results.getJSONObject(i).getString("runtime"));

                                JSONObject iconObject = new JSONObject(results.getJSONObject(i).getString("image"));
                                smallIconUrls.add(i, iconObject.getString("small_url"));

                                /*JSONArray studiosArray = results.getJSONObject(i).getJSONArray("studios");
                                //JSONArray studiosArray = new JSONArray(respuestaJSON.getString("studios"));

                                if(studiosArray != null){

                                    String s="";
                                    for(int j=0; j<studiosArray.length(); j++){

                                        //JSONObject studioObject = new JSONObject(studiosArray.get(j).toString());
                                        s.concat( studiosArray.getJSONObject(j).getString("name"));
                                        s.concat(", ");
                                    }
                                    studios.add(i,s);

                                } else
                                    studios.add(i,"None");*/

                                revenues.add(i,results.getJSONObject(i).getString("total_revenue"));

                                /*JSONArray writersArray = results.getJSONObject(i).getJSONArray("writers");
                                //JSONArray writersArray = new JSONArray(respuestaJSON.getString("writers"));

                                if(writersArray != null){

                                    String s="";
                                    for(int j=0; j<writersArray.length(); j++){

                                        //JSONObject writerObject = new JSONObject(writersArray.get(j).toString());
                                        s.concat( writersArray.getJSONObject(j).getString("name"));
                                        s.concat(", ");
                                    }
                                    writers.add(i,s);

                                } else
                                    writers.add(i,"None");*/
                            }
                        }

                    } else // resultJSON != OK
                        error = 2;
                } else  // HTTP_NOT_OK
                    error = 1;

            } catch (MalformedURLException e){
                e.printStackTrace();
            } catch (IOException e){
                e.printStackTrace();
            } catch (JSONException e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onCancelled(String s) {
            super.onCancelled(s);
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            progressDialog.dismiss();


            adapter = new RecyclerForFavorites(context, descriptions, ids, names, producers, ratings, releaseDates, runtimes, smallIconUrls, studios, revenues, writers);
            recyclerView.setAdapter(adapter);
            animations.runRecyclerAnimation(recyclerView);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = ProgressDialog.show(context, "Cargando lista de favoritas","Por favor, espere...",false,false);}
    }
}
