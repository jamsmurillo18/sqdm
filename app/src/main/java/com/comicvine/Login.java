package com.comicvine;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import android.widget.Toast;

import com.comicvine.Tools.Preference;

public class Login extends AppCompatActivity {

    private LinearLayout linearLayout;
    private EditText emailEt, passEt;
    private TextView loginTitle, noAccountYet;
    private Button loginBut, listBut;

    private Preference preference = new Preference();
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        declareAll();
        Typeface myFontT = Typeface.createFromAsset(getAssets(), "fonts/calibri_bold_italic.ttf");
        loginTitle.setTypeface(myFontT);

        loginBut.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                preference.setUserParam(getSupportActionBar().getThemedContext(),
                        "0",
                        "John",
                        "Murillo",
                        "jamsmurillo92@gmail.com",
                        "1234");

                Intent main = new Intent(getSupportActionBar().getThemedContext(),Main.class);
                main.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(main);

                /*if(allIsCorrect()){

                    if(rs != null){

                        preference.setUserParam(getSupportActionBar().getThemedContext(),
                                rs.getString(rs.getColumnIndex(DBHelper.CONTACTS_COLUMN_ID)),
                                rs.getString(rs.getColumnIndex(DBHelper.CONTACTS_COLUMN_NAME)),
                                rs.getString(rs.getColumnIndex(DBHelper.CONTACTS_COLUMN_LAST)),
                                rs.getString(rs.getColumnIndex(DBHelper.CONTACTS_COLUMN_EMAIL)),
                                rs.getString(rs.getColumnIndex(DBHelper.CONTACTS_COLUMN_PASSWORD)));

                        Intent main = new Intent(getSupportActionBar().getThemedContext(),Main.class);
                        main.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(main);

                    } else
                        Toast.makeText(getSupportActionBar().getThemedContext(),
                        "No existe una cuenta asociada a este correo electrónico, por favor seleccione Registrate aquí", Toast.LENGTH_LONG).show();

                } else
                    Toast.makeText(getSupportActionBar().getThemedContext(), "Escribe tu correo electronico y tu contraseña", Toast.LENGTH_LONG).show();*/
            }
        });

        listBut.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                Intent movieList = new Intent(getSupportActionBar().getThemedContext(), MovieList.class);
                startActivity(movieList);
            }
        });

        noAccountYet.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent register = new Intent(getSupportActionBar().getThemedContext(), Register.class);
                startActivity(register);
            }
        });

    }

    public void declareAll(){

        linearLayout = (LinearLayout)findViewById(R.id.mainLayout);

        loginTitle = (TextView)findViewById(R.id.login_title);

        emailEt = (EditText)findViewById(R.id.doc_et);
        passEt = (EditText)findViewById(R.id.password_et);

        loginBut = (Button)findViewById(R.id.login_but);
        listBut = (Button)findViewById(R.id.list_but);
        noAccountYet = (TextView)findViewById(R.id.noAccountYet);
    }

    private boolean allIsCorrect(){

        boolean correct = true;

        if (emailEt.getText().toString().length()==0)
            correct = (correct && false);

        if(passEt.getText().toString().length()==0)
            correct = (correct && false);

        return correct;
    }
}
