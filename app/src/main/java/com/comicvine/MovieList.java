package com.comicvine;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.widget.TextView;

import com.comicvine.Adapters.RecyclerForMovieList;
import com.comicvine.Tools.Animations;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MovieList extends AppCompatActivity {

    List<String> smallIconUrls = new ArrayList<>();
    List<String> names = new ArrayList<>();

    RecyclerForMovieList adapter;
    RecyclerView recyclerView;
    Animations animations = new Animations();

    Context context;
    getMovieService wireConnection;
    String CALL = "https://comicvine.gamespot.com/api/movies/?api_key=b92fe4a1f037e397515a6b759239eaee558eef53&format=json";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Typeface myFontT = Typeface.createFromAsset(getAssets(), "fonts/calibri_bold_italic.ttf");
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setTypeface(myFontT);

        context = getSupportActionBar().getThemedContext();
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        int noOfColumns = (int) (dpWidth / 160);

        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(noOfColumns+1, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);

        ConnectivityManager connMgr = (ConnectivityManager)
                getSupportActionBar().getThemedContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {

            wireConnection = new getMovieService();
            wireConnection.execute(CALL);
        }
    }

    public class getMovieService extends AsyncTask<String, Void, String> {

        int error;
        ProgressDialog progressDialog;

        protected String doInBackground(String... params) {

            URL url = null ;

            try{
                url = new URL(params[0]);

                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestProperty("Accept-Encoding","identity");

                int rta = connection.getResponseCode();
                StringBuilder result = new StringBuilder();

                error = 3;

                if(rta == HttpURLConnection.HTTP_OK){

                    InputStream in = new BufferedInputStream(connection.getInputStream());
                    BufferedReader reader = new BufferedReader(new InputStreamReader(in));

                    String line;
                    while ((line = reader.readLine()) != null){
                        result.append(line);
                    }

                    JSONObject respuestaJSON = new JSONObject(result.toString());
                    String resultJSON = respuestaJSON.getString("error");

                    if(resultJSON.equals("OK")){

                        error = 4;
                        names.clear();
                        smallIconUrls.clear();

                        JSONArray results = respuestaJSON.getJSONArray("results");

                        if(results != null){

                            for(int i=0; i<results.length(); i++){

                                names.add(i,results.getJSONObject(i).getString("name"));

                                JSONObject iconObject = new JSONObject(results.getJSONObject(i).getString("image"));
                                smallIconUrls.add(i, iconObject.getString("small_url"));
                            }
                        }

                    } else // resultJSON != OK
                        error = 2;
                } else  // HTTP_NOT_OK
                    error = 1;

            } catch (MalformedURLException e){
                e.printStackTrace();
            } catch (IOException e){
                e.printStackTrace();
            } catch (JSONException e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onCancelled(String s) {
            super.onCancelled(s);
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            progressDialog.dismiss();

            adapter = new RecyclerForMovieList(context, smallIconUrls, names);
            recyclerView.setAdapter(adapter);
            animations.runRecyclerAnimation(recyclerView);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = ProgressDialog.show(MovieList.this, "Cargando lista de películas","Por favor, espere...",false,false);}
    }
}
