package com.comicvine;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.comicvine.Fragments.Fragment_account;
import com.comicvine.Fragments.Fragment_favorites;
import com.comicvine.Fragments.Fragment_movies;
import com.comicvine.Tools.Animations;
import com.comicvine.Tools.CircledImageView;
import com.comicvine.Tools.CustomPrompt;
import com.comicvine.Tools.Preference;
import com.comicvine.About;

import java.io.File;

public class Main extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    Preference preference = new Preference();
    Animations animations = new Animations();
    ProgressDialog progressDialog;
    static FragmentManager fragmentManager;
    static Fragment fragment;
    DrawerLayout drawer;
    String[] types;
    int typeChecked =-1;


    String CALL = "";
    int error = 0;
    static Context context;
    Context context1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

        Typeface myFontT = Typeface.createFromAsset(getAssets(), "fonts/calibri_bold_italic.ttf");
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setTypeface(myFontT);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View headerView = navigationView.inflateHeaderView(R.layout.nav_header_main);
        navigationView.setCheckedItem(R.id.nav_movies);

        CircledImageView imageView = (CircledImageView) headerView.findViewById(R.id.imageView);
        TextView header = (TextView)headerView.findViewById(R.id.header_title);
        TextView email = (TextView)headerView.findViewById(R.id.email_tv);

        String filePath = preference.getUserProfilePicturePath(getSupportActionBar().getThemedContext());
        File dir = new File(filePath);
        if(dir.exists())
            imageView.setImageBitmap(BitmapFactory.decodeFile(filePath));
        header.setText(new Preference().getUserParam(getSupportActionBar().getThemedContext(),1)
                + " " + new Preference().getUserParam(getSupportActionBar().getThemedContext(),2));
        email.setText(new Preference().getUserParam(getSupportActionBar().getThemedContext(),3));

        context = getSupportActionBar().getThemedContext();
        fragment = new Fragment_movies();
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.contenedorFrame,fragment).commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")

    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.nav_account) {

            fragment = new Fragment_account();
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            fragmentManager.beginTransaction().replace(R.id.contenedorFrame,fragment).commit();

        } else if (id == R.id.nav_movies) {

            fragment = new Fragment_movies();
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            fragmentManager.beginTransaction().replace(R.id.contenedorFrame,fragment).commit();

        } else if (id == R.id.nav_favorites) {

            fragment = new Fragment_favorites();
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            fragmentManager.beginTransaction().replace(R.id.contenedorFrame,fragment).commit();

        } else if (id == R.id.nav_insta) {

            startActivity(newInstagramProfileIntent(getSupportActionBar().getThemedContext().getPackageManager(),"http://instagram.com/comicvine"));

        } else if (id == R.id.nav_share) {

            Intent shareApp = new Intent();
            shareApp.setAction(Intent.ACTION_SEND);
            shareApp.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id="
                    + getSupportActionBar().getThemedContext().getPackageName());
            shareApp.setType("text/plain");
            startActivity(Intent.createChooser(shareApp, "Compartir aplicación en"));

        } else if (id == R.id.nav_qualify) {

            Handler mHandler = new Handler();
            mHandler.postDelayed(new Runnable() {
                public void run() {
                    CustomPrompt customPrompt = new CustomPrompt();
                    customPrompt.myToast(getSupportActionBar().getThemedContext(),
                            getResources().getString(R.string.msg_slide_up), R.drawable.ic_slideup,true);
                    customPrompt.myToast(getSupportActionBar().getThemedContext(),
                            getResources().getString(R.string.msg_slide_up), R.drawable.ic_slideup,true);
                }
            }, 2000);

            Intent goToMarket = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id="
                    +getSupportActionBar().getThemedContext().getPackageName()));
            goToMarket.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            try{
                startActivity(goToMarket);
            }catch (ActivityNotFoundException e){
                startActivity(new Intent(Intent.ACTION_VIEW,Uri.parse("http://play.google.com/store/app/details?id="
                        +getSupportActionBar().getThemedContext().getPackageName())));
            }

        } else if (id == R.id.nav_about) {

            Intent about = new Intent(getSupportActionBar().getThemedContext(),About.class);
            startActivity(about);

        } else if (id == R.id.nav_contact_dev) {

            /*Intent contactDev = new Intent(getSupportActionBar().getThemedContext(),ContactDev.class);
            startActivity(contactDev);*/

        } else if (id == R.id.nav_log_out) {

            new Preference().clearParam(getSupportActionBar().getThemedContext());
            Intent login = new Intent(getSupportActionBar().getThemedContext(),Login.class);
            login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(login);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public static Intent newInstagramProfileIntent(PackageManager pm, String url) {
        final Intent intent = new Intent(Intent.ACTION_VIEW);
        try {
            if (pm.getPackageInfo("com.instagram.android", 0) != null) {
                if (url.endsWith("/")) {
                    url = url.substring(0, url.length() - 1);
                }
                final String username = url.substring(url.lastIndexOf("/") + 1);
                intent.setData(Uri.parse("http://instagram.com/_u/" + username));
                intent.setPackage("com.instagram.android");
                return intent;
            }
        } catch (PackageManager.NameNotFoundException ignored) {
        }
        intent.setData(Uri.parse(url));
        return intent;
    }
}
