package com.comicvine.Adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.comicvine.R;
import com.comicvine.Tools.Preference;
import com.squareup.picasso.Picasso;

public class RecyclerForFavorites extends RecyclerView.Adapter<RecyclerForFavorites.ViewHolder> {

    private Context context;
    private List<String> descriptions = new ArrayList<>();
    private List<String> ids = new ArrayList<>();
    private List<String> names = new ArrayList<>();
    private List<String> producers = new ArrayList<>();
    private List<String> ratings = new ArrayList<>();
    private List<String> releaseDates = new ArrayList<>();
    private List<String> runtimes = new ArrayList<>();
    private List<String> smallIconUrls = new ArrayList<>();
    private List<String> studios = new ArrayList<>();
    private List<String> revenues = new ArrayList<>();
    private List<String> writers = new ArrayList<>();

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        //public LinearLayout layout;
        public ImageView movieIcon;
        public TextView descripTxt;
        public TextView titleTxt;

        public ViewHolder(View v) {
            super(v);

            //layout = (LinearLayout) v.findViewById(R.id.layout);
            movieIcon = (ImageView) v.findViewById(R.id.iconIv);
            titleTxt = (TextView) v.findViewById(R.id.title_txt);
            descripTxt = (TextView) v.findViewById(R.id.subtitle_txt);
            v.setClickable(true);
            v.setOnClickListener(this);
        }

        public void onClick(final View v) {


        }
    }

    public void add(int position, String descrip, String id, String name, String producer, String rating, String releaseDate,
                    String runtime, String image, String studio, String revenue, String writer) {

        descriptions.add(position, descrip);
        ids.add(position, id);
        names.add(position, name);
        //producers.add(position, producer);
        ratings.add(position, rating);
        releaseDates.add(position, releaseDate);
        runtimes.add(position, runtime);
        smallIconUrls.add(position, image);
        //studios.add(position, studio);
        revenues.add(position, revenue);
        //writers.add(position, writer);
        notifyItemInserted(position);
    }

    public void remove(int position) {

        descriptions.remove(position);
        ids.remove(position);
        names.remove(position);
        //producers.remove(position);
        ratings.remove(position);
        releaseDates.remove(position);
        runtimes.remove(position);
        smallIconUrls.remove(position);
        //studios.remove(position);
        revenues.remove(position);
        //writers.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, descriptions.size());
        notifyItemRangeChanged(position, ids.size());
        notifyItemRangeChanged(position, names.size());
        //notifyItemRangeChanged(position, producers.size());
        notifyItemRangeChanged(position, ratings.size());
        notifyItemRangeChanged(position, releaseDates.size());
        notifyItemRangeChanged(position, runtimes.size());
        notifyItemRangeChanged(position, smallIconUrls.size());
        //notifyItemRangeChanged(position, studios.size());
        notifyItemRangeChanged(position, revenues.size());
        //notifyItemRangeChanged(position, writers.size());
    }

    public RecyclerForFavorites(Context c, List<String> descriptions, List<String> ids, List<String> names, List<String> producers, List<String> ratings,
                                List<String> releaseDates, List<String> runtimes, List<String> smallIconUrls, List<String> studios,
                                List<String> revenues, List<String> writers) {

        context = c;
        this.descriptions = descriptions;
        this.ids = ids;
        this.names = names;
        //this.producers = producers;
        this.ratings = ratings;
        this.releaseDates = releaseDates;
        this.runtimes = runtimes;
        this.smallIconUrls = smallIconUrls;
        //this.studios = studios;
        this.revenues = revenues;
        //this.writers = writers;
    }

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_favorites, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    public void onBindViewHolder(final ViewHolder holder, int position) {

        Picasso.with(context)
                .load(smallIconUrls.get(position))
                .resize(432,640)
                .into(holder.movieIcon);

        holder.descripTxt.setText(runtimes.get(position) + " min");
        holder.titleTxt.setText(names.get(position));
    }

    public int getItemCount() {
        return ids.size();
    }
}