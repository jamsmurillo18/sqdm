package com.comicvine.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.comicvine.Detail;
import com.comicvine.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class RecyclerForMovieList extends RecyclerView.Adapter<RecyclerForMovieList.ViewHolder> {

    private Context context;
    private List<String> names = new ArrayList<>();
    private List<String> smallIconUrls = new ArrayList<>();

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView movieIcon;
        public TextView titleTxt;

        public ViewHolder(View v) {
            super(v);

            movieIcon = (ImageView) v.findViewById(R.id.iconIv);
            titleTxt = (TextView) v.findViewById(R.id.title_txt);
            v.setClickable(true);
            v.setOnClickListener(this);
        }

        public void onClick(final View v) {

            Toast.makeText(v.getContext(), "Para mayor información y contenido, por favor Inicia sesión. Si no tienes cuenta, por favor Regístrate", Toast.LENGTH_SHORT).show();
        }
    }

    public void add(int position, String image, String name) {

        smallIconUrls.add(position, image);
        names.add(position, name);
        notifyItemInserted(position);
    }

    public void remove(int position) {

        names.remove(position);
        smallIconUrls.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, names.size());
        notifyItemRangeChanged(position, smallIconUrls.size());
    }

    public RecyclerForMovieList(Context c, List<String> smallIconUrls, List<String> names) {

        context = c;
        this.names = names;
        this.smallIconUrls = smallIconUrls;
    }

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_movie_list, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    public void onBindViewHolder(final ViewHolder holder, int position) {

        Picasso.with(context)
                .load(smallIconUrls.get(position))
                .resize(432,640)
                .into(holder.movieIcon);

        holder.titleTxt.setText(names.get(position));
    }

    public int getItemCount() {
        return names.size();
    }
}