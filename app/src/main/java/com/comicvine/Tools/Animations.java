package com.comicvine.Tools;

import android.animation.Animator;
import android.content.Context;
import android.os.Build;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.view.animation.LayoutAnimationController;

import com.comicvine.R;

public class Animations {

    public Animations() {
        super();
    }

    public void animateFAB(Context context, FloatingActionButton floatingActionButton) {

        floatingActionButton.setScaleX(0);
        floatingActionButton.setScaleY(0);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            final Interpolator interpolador = AnimationUtils.loadInterpolator(context, android.R.interpolator.fast_out_linear_in);

            floatingActionButton.animate().scaleX(1).scaleY(1).setInterpolator(interpolador).setDuration(500).setStartDelay(500).setListener(new Animator.AnimatorListener() {

                public void onAnimationStart(Animator animation) { }

                public void onAnimationEnd(Animator animation) { }

                public void onAnimationCancel(Animator animation) { }

                public void onAnimationRepeat(Animator animation) { }
            });
        }
    }

    public void runRecyclerAnimation(RecyclerView recyclerView) {

        Context context = recyclerView.getContext();
        LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(context, R.anim.recycler_item_anim);

        controller = AnimationUtils.loadLayoutAnimation(context, R.anim.recycler_item_anim);

        recyclerView.setLayoutAnimation(controller);
        recyclerView.getAdapter().notifyDataSetChanged();
        recyclerView.scheduleLayoutAnimation();
    }
}