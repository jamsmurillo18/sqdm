package com.comicvine.Tools;

import android.content.Context;
import android.content.SharedPreferences;

public class Preference {

    public static final String MAIN = "MAIN_PREFS";
    public static final String USER_ID = "USER_ID";
    public static final String USER_NAME = "USER_NAME";
    public static final String USER_LAST = "USER_LAST";
    public static final String USER_EMAIL = "USER_EMAIL";
    public static final String USER_PASSWORD = "USER_PASSWORD";
    public static final String USER_PROFILE_PICTURE_PATH = "USER_PROFILE_PICTURE_PATH";

    public Preference() {
        super();
    }

    public void setUserParam(Context context, String... params) {

        SharedPreferences main;
        SharedPreferences.Editor main_editor;
        main = context.getSharedPreferences(MAIN, context.MODE_PRIVATE);
        main_editor = main.edit();
        main_editor.putString(USER_ID, params[0]);
        main_editor.putString(USER_NAME, params[1]);
        main_editor.putString(USER_LAST, params[2]);
        main_editor.putString(USER_EMAIL, params[3]);
        main_editor.putString(USER_PASSWORD, params[4]);
        main_editor.commit();
    }

    public String getUserParam(Context context, int whichParam) {

        String string = "";
        SharedPreferences main;
        main = context.getSharedPreferences(MAIN, context.MODE_PRIVATE);

        if (whichParam == 0)
            string = main.getString(USER_ID,"");
        else if (whichParam == 1)
            string = main.getString(USER_NAME,"");
        else if (whichParam == 2)
            string = main.getString(USER_LAST,"");
        else if (whichParam == 3)
            string = main.getString(USER_EMAIL,"");
        else if (whichParam == 4)
            string = main.getString(USER_PASSWORD,"");

        return string;
    }

    public void setUserProfilePicturePath(Context context, String path) {

        SharedPreferences main;
        SharedPreferences.Editor main_editor;
        main = context.getSharedPreferences(MAIN, context.MODE_PRIVATE);
        main_editor = main.edit();
        main_editor.putString(USER_PROFILE_PICTURE_PATH, path);
        main_editor.commit();
    }

    public String getUserProfilePicturePath(Context context) {

        String string = "";
        SharedPreferences main;
        main = context.getSharedPreferences(MAIN, context.MODE_PRIVATE);

        string = main.getString(USER_PROFILE_PICTURE_PATH,"");

        return string;
    }

    public void clearParam(Context context){

        SharedPreferences main;
        SharedPreferences.Editor main_editor;
        main = context.getSharedPreferences(MAIN, context.MODE_PRIVATE);
        main_editor = main.edit();
        main_editor.clear();
        main_editor.commit();
    }
}