package com.comicvine.Tools;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.comicvine.R;

public class CustomPrompt {

    public CustomPrompt() { super();}

    public void myToast(Context context, String content, int imgResource, boolean isCompound){

        LayoutInflater inflater = (LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        final LinearLayout layout;

        if(isCompound){
            layout = (LinearLayout) inflater.inflate(R.layout.layout_compound_toast, null);
            ImageView iv_notice = (ImageView) layout.findViewById(R.id.iv_ad);
            iv_notice.setImageResource(imgResource);
        }
        else
            layout = (LinearLayout) inflater.inflate(R.layout.layout_simple_toast, null);

        TextView tv_notice = (TextView) layout.findViewById(R.id.tv_ad);
        tv_notice.setText(content);

        Toast mToast = new Toast(context);
        mToast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL, 0, 0);
        //mToast.setGravity(Gravity.CENTER | Gravity.FILL_HORIZONTAL, 0, 0);
        mToast.setDuration(Toast.LENGTH_LONG);
        mToast.setView(layout);
        mToast.show();
    }
}
