package com.comicvine.DataBase;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import android.graphics.Movie;

import java.util.List;

@Dao

public interface MovieDao {

    @Query("SELECT * FROM movies")
    List<Movie> getAll();

    @Insert
    void insert(Movie movie);

    @Insert
    void insertMultipleMovies (List<Movies> moviesList);
    @Query ("SELECT * FROM Movies WHERE movieId = :movieId")
    Movies fetchOneMoviesbyMovieId (int movieId);

    @Delete
    void delete(Movie movie);

    @Update
    void update(Movie movie);
}
