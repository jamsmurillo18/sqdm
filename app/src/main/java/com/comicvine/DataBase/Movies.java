package com.comicvine.DataBase;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity
public class Movies {
    @NonNull
    @PrimaryKey
    private String movieId;
    private String movieName;
    private String movieDescription;
    private String movieProducer;
    private String movieRating;
    private String movieReleaseDate;
    private String movieRuntime;
    private String movieImageUrl;
    private String movieStudios;
    private String movieRevenues;
    private String movieWriters;

    public Movies() {
    }

    public String getMovieId() { return movieId; }

    public void setMovieId(String movieId) { this.movieId = movieId; }

    public String getMovieName() { return movieName; }

    public void setMovieName (String movieName) { this.movieName = movieName; }

    public String getMovieDescription() { return movieDescription; }

    public void setMovieDescription(String movieDescription) { this.movieId = movieDescription; }

    public String getMovieProducers() { return movieProducer; }

    public void setMovieProducers (String movieProducer) { this.movieName = movieProducer; }

    public String getMovieRating() { return movieRating; }

    public void setMovieRating(String movieRating) { this.movieRating = movieRating; }

    public String getMovieReleaseDate() { return movieReleaseDate; }

    public void setMovieReleaseDate (String movieReleaseDate) { this.movieReleaseDate = movieReleaseDate; }

    public String getMovieRuntime() { return movieRuntime; }

    public void setMovieRuntime(String movieRuntime) { this.movieRuntime = movieRuntime; }

    public String getMovieImageUrl() { return movieImageUrl; }

    public void setMovieImageUrl (String movieImageUrl) { this.movieImageUrl = movieImageUrl; }

    public String getMovieStudios() { return movieStudios; }

    public void setMovieStudios (String movieStudios) { this.movieStudios = movieStudios; }

    public String getMovieRevenues() { return movieRevenues; }

    public void setMovieRevenues(String movieRevenues) { this.movieRevenues = movieRevenues; }

    public String getMovieWriters() { return movieWriters; }

    public void setMovieWriters (String movieWriters) { this.movieWriters = movieWriters; }
}