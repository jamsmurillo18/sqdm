package com.comicvine;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.WindowManager;
import android.widget.TextView;

import com.comicvine.Tools.Preference;

public class Splash extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        Handler mHandler = new Handler();
        mHandler.postDelayed(new Runnable() {
            public void run() {

                if(new Preference().getUserParam(getSupportActionBar().getThemedContext(),3) != ""){

                    Intent main = new Intent(getSupportActionBar().getThemedContext(),Main.class);
                    startActivity(main);
                    finish();

                } else {

                    Intent login = new Intent(getSupportActionBar().getThemedContext(),Login.class);
                    startActivity(login);
                    finish();
                }
            }
        }, 1750);
    }
}
