La app tiene por nombre Comic Vine como el sitio web del que proviene la información.

Presenta un splash screen para empezar su uso y luego una interfaz de LOGIN donde se da la opción al usuario de

1)Iniciar sesión: el comoponente button está definido para que (sin necesidad de ingresar datos de correo electrónico y contraseña) se pueda
observar el comportamiento de la app frente a un usuario que ya se ha registrado previamente e inicia sesión (la app crea un usuario de prueba).

2)Ver películas: al seleccionar esta opción se podrá ver una lista de películas con sus respectivos nombres (que en la mayoría de casos 
no se muestran completos) y la imagen promocional de la película. cuando el usuario seleccione cualquier película para conocer más detalles, la app
muestra un mensaje motivando al usuario a registrarse o iniciar sesión.

3)Regístrate aquí: seleccionar esta opción lleva al usuario a una interfaz de registro de datos sencillos.
Pide que se completen todos los campos para efectos de prueba.

Al iniciar sesión, se observa en la interfaz principal una lista de películas disponibles, se observa la imagen promocional de la película,
el nombre y una descripción de la misma. Seleccionar una película lleva a la interfaz de detalle de la película donde se pueden apreciar más información
como la calificación de la película, fecha y hora de lanzamiento, duración en minutos e ingresos recaudados.

Se puede apreciar un menu lateral (DrawerLayout) con las opciones:

4)Mi cuenta: aquí se pueden gestionar los datos de usuario y su foto de perfil.

5)Películas: la lista de películas completas que se puede encontrar en Comic Vine.

6)Instagram: con la intensión de promocionar una de las redes sociales de Comic Vine se incluyó un acceso directo al perfil de instagram.

7)Compartir: esta función permite compartir el link (a cualquier contacto) de descarga de la app en Play Store.

8)Me gusta: a fin de conocer la apreciación de los usuarios respecto a la app, esta función lleva al usuario a valorar la app en Play Store.

9)Acerca de:  información indispensable en todo software.

10)Cerrar sesión: permite comenzar nuevamente la prueba del uso de la app.

11)Contactar desarrolladores: a fin de promover la fábrica de software, se incluyó una función de contacto directo con los desarrolladores.